package sam3

import (
	"fmt"
	"testing"
)

const yoursam = "127.0.0.1:7656"

func Test_Basic(t *testing.T) {
	fmt.Println("Test_Basic")
	fmt.Println("\tAttaching to SAM at " + yoursam)
	sam, err := NewSAM(yoursam)
	if err != nil {
		fmt.Println(err.Error())
		t.Fail()
		return
	}

	fmt.Println("\tCreating new keys...")
	keys, err := sam.NewKeys()
	if err != nil {
		fmt.Println(err.Error())
		t.Fail()
	} else {
		fmt.Println("\tAddress created: " + keys.Addr().Base32())
		fmt.Println("\tI2PKeys: " + string(keys.both)[:50] + "(...etc)")
	}

	addr2, err := sam.Lookup("zzz.i2p")
	if err != nil {
		fmt.Println(err.Error())
		t.Fail()
	} else {
		fmt.Println("\tzzz.i2p = " + addr2.Base32())
	}

	if err := sam.Close(); err != nil {
		fmt.Println(err.Error())
		t.Fail()
	}
}

/*
func Test_GenericSession(t *testing.T) {
	if testing.Short() {
		return
	}
	fmt.Println("Test_GenericSession")
	sam, err := NewSAM(yoursam)
	if err != nil {
		fmt.Println(err.Error)
		t.Fail()
		return
	}
	keys, err := sam.NewKeys()
	if err != nil {
		fmt.Println(err.Error())
		t.Fail()
	} else {
		conn1, err := sam.newGenericSession("STREAM", "testTun", keys, []string{})
		if err != nil {
			fmt.Println(err.Error())
			t.Fail()
		} else {
			conn1.Close()
		}
		conn2, err := sam.newGenericSession("STREAM", "testTun", keys, []string{"inbound.length=1", "outbound.length=1", "inbound.lengthVariance=1", "outbound.lengthVariance=1", "inbound.quantity=1", "outbound.quantity=1"})
		if err != nil {
			fmt.Println(err.Error())
			t.Fail()
		} else {
			conn2.Close()
		}
		conn3, err := sam.newGenericSession("DATAGRAM", "testTun", keys, []string{"inbound.length=1", "outbound.length=1", "inbound.lengthVariance=1", "outbound.lengthVariance=1", "inbound.quantity=1", "outbound.quantity=1"})
		if err != nil {
			fmt.Println(err.Error())
			t.Fail()
		} else {
			conn3.Close()
		}
	}
	if err := sam.Close(); err != nil {
		fmt.Println(err.Error())
		t.Fail()
	}
}
*/
