package sam3

import (
	"fmt"
	"testing"
)

func Test_MasterServerClient(t *testing.T) {
	if testing.Short() {
		return
	}

	fmt.Println("Test_MasterServerClient")
	sam, err := NewSAM(yoursam)
	if err != nil {
		t.Fail()
		return
	}
	defer sam.Close()
	keys, err := sam.NewKeys()
	if err != nil {
		t.Fail()
		return
	}
	fmt.Println("\tServer: Creating tunnel")
	ms, err := sam.NewMasterSession("MserverTun", keys, []string{"inbound.length=0", "outbound.length=0", "inbound.lengthVariance=0", "outbound.lengthVariance=0", "inbound.quantity=1", "outbound.quantity=1"}, 0)
	if err != nil {
		fmt.Println("Server: Failed to create tunnel: " + err.Error())
		t.Fail()
		return
	}
	defer ms.Close()
	fmt.Println("\tServer: Adding sessions")
	ds, err := ms.AddDatagramSession("DGserverTun", []string{})
	if err != nil {
		fmt.Println("\tServer: Failed to add datagram session: " + err.Error())
		t.Fail()
		return
	}
	rs, err := ms.AddRawSession("RAWserverTun", []string{})
	if err != nil {
		fmt.Println("\tServer: Failed to add raw session: " + err.Error())
		t.Fail()
		return
	}

	sam2, err := NewSAM(yoursam)
	if err != nil {
		t.Fail()
		return
	}
	defer sam2.Close()
	keys, err = sam2.NewKeys()
	if err != nil {
		t.Fail()
		return
	}
	fmt.Println("\tClient: Creating tunnel")
	ms2, err := sam2.NewMasterSession("MclientTun", keys, []string{"inbound.length=0", "outbound.length=0", "inbound.lengthVariance=0", "outbound.lengthVariance=0", "inbound.quantity=1", "outbound.quantity=1"}, 0)
	if err != nil {
		fmt.Println("\tClient: Failed to create tunnel: " + err.Error())
		t.Fail()
		return
	}
	defer ms2.Close()
	fmt.Println("\tClient: Adding sessions")
	ds2, err := ms2.AddDatagramSession("DGclientTun", []string{})
	if err != nil {
		fmt.Println("\tClient: Failed to add datagram session: " + err.Error())
		t.Fail()
		return
	}
	rs2, err := ms2.AddRawSession("RAWclientTun", []string{}) // for extra anonymity
	if err != nil {
		fmt.Println("\tClient: Failed to add raw session: " + err.Error())
		t.Fail()
		return
	}
	fmt.Println("\tClient: Tries to send datagrams to server")

	_, err = ds2.WriteTo([]byte("Hello datagram-world! <3 <3 <3 <3 <3 <3"), ds.LocalAddr())
	if err != nil {
		fmt.Println("\tClient: Failed to send datagram: " + err.Error())
		t.Fail()
		return
	}
	_, err = rs2.WriteTo([]byte("Hello raw-world! <3 <3 <3 <3 <3 <3"), rs.LocalAddr())
	if err != nil {
		fmt.Println("\tClient: Failed to send raw datagram: " + err.Error())
		t.Fail()
		return
	}

	buf := make([]byte, 512)
	fmt.Println("\tServer: ReadFrom() waiting...")
	n, _, err := ds.ReadFrom(buf)
	if err != nil {
		fmt.Println("\tServer: Failed to ReadFrom(): " + err.Error())
		t.Fail()
		return
	}
	fmt.Println("\tServer: Received datagram: " + string(buf[:n]))
	n, err = rs.Read(buf)
	if err != nil {
		fmt.Println("\tServer: Failed to ReadFrom(): " + err.Error())
		t.Fail()
		return
	}
	fmt.Println("\tServer: Received raw datagram: " + string(buf[:n]))
}
