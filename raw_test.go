package sam3

import (
	"fmt"
	"testing"
)

func Test_RawServerClient(t *testing.T) {
	if testing.Short() {
		return
	}

	fmt.Println("Test_RawServerClient")
	sam, err := NewSAM(yoursam)
	if err != nil {
		t.Fail()
		return
	}
	defer sam.Close()
	keys, err := sam.NewKeys()
	if err != nil {
		t.Fail()
		return
	}
	fmt.Println("\tServer: Creating tunnel")
	rs, err := sam.NewRawSession("RAWserverTun", keys, []string{"inbound.length=0", "outbound.length=0", "inbound.lengthVariance=0", "outbound.lengthVariance=0", "inbound.quantity=1", "outbound.quantity=1"}, 0)
	if err != nil {
		fmt.Println("Server: Failed to create tunnel: " + err.Error())
		t.Fail()
		return
	}

	sam2, err := NewSAM(yoursam)
	if err != nil {
		t.Fail()
		return
	}
	defer sam2.Close()
	keys, err = sam2.NewKeys()
	if err != nil {
		t.Fail()
		return
	}
	fmt.Println("\tClient: Creating tunnel")
	rs2, err := sam2.NewRawSession("RAWclientTun", keys, []string{"inbound.length=0", "outbound.length=0", "inbound.lengthVariance=0", "outbound.lengthVariance=0", "inbound.quantity=1", "outbound.quantity=1"}, 0)
	if err != nil {
		fmt.Println("\tClient: Failed to create tunnel: " + err.Error())
		t.Fail()
		return
	}
	defer rs2.Close()
	fmt.Println("\tClient: Tries to send raw datagram to server")

	_, err = rs2.WriteTo([]byte("Hello raw-world! <3 <3 <3 <3 <3 <3"), rs.LocalAddr())
	if err != nil {
		fmt.Println("\tClient: Failed to send raw datagram: " + err.Error())
		t.Fail()
		return
	}
	buf := make([]byte, 512)
	fmt.Println("\tServer: Read() waiting...")
	n, err := rs.Read(buf)
	if err != nil {
		fmt.Println("\tServer: Failed to Read(): " + err.Error())
		t.Fail()
		return
	}
	fmt.Println("\tServer: Received raw datagram: " + string(buf[:n]))
}
