package sam3

import (
	"errors"
	"net"
	"regexp"
	"strconv"
	"strings"
)

type MasterSession struct {
	sam         SAM          // sam bridge
	id          string       // tunnel name
	keys        I2PKeys      // i2p destination keys
	rUDPAddr    *net.UDPAddr // the SAM bridge UDP-port
	subsessions map[string]closable
}

type closable interface {
	Close() error
}

func (s *SAM) NewMasterSession(id string, keys I2PKeys, options []string, udpPort int) (*MasterSession, error) {
	if udpPort > 65335 || udpPort < 0 {
		return nil, errors.New("udpPort must be in the interval 0-65335")
	}
	if udpPort == 0 {
		udpPort = 7655
	}
	rhost, _, err := net.SplitHostPort(s.conn.RemoteAddr().String())
	if err != nil {
		s.Close()
		return nil, err
	}
	rUDPAddr, err := net.ResolveUDPAddr("udp4", rhost+":"+strconv.Itoa(udpPort))
	if err != nil {
		return nil, err
	}
	_, err = s.newGenericSession("MASTER", id, keys, options, []string{})
	if err != nil {
		return nil, err
	}
	return &MasterSession{*s, id, keys, rUDPAddr, make(map[string]closable)}, nil
}

func (s *MasterSession) addGenericSession(style, id string, options []string, extras []string) error { // no option to override / provide additional options?
	conn := s.sam.conn
	scmsg := []byte("SESSION ADD STYLE=" + style + " ID=" + id + " " + strings.Join(options, " ") + strings.Join(extras, " ") + "\n")
	for m, i := 0, 0; m != len(scmsg); i++ {
		if i == 15 {
			conn.Close()
			return errors.New("writing to SAM failed")
		}
		n, err := conn.Write(scmsg[m:])
		if err != nil {
			conn.Close()
			return err
		}
		m += n
	}
	buf := make([]byte, 4096)
	n, err := conn.Read(buf)
	if err != nil {
		conn.Close()
		return err
	}
	text := string(buf[:n])
	if strings.HasPrefix(text, session_OK) {
		return nil
	} else if text == session_DUPLICATE_ID {
		conn.Close()
		return errors.New("duplicate tunnel name")
	} else if text == session_DUPLICATE_DEST {
		conn.Close()
		return errors.New("duplicate destination")
	} else if text == session_INVALID_KEY {
		conn.Close()
		return errors.New("invalid key")
	} else if strings.HasPrefix(text, session_I2P_ERROR) {
		conn.Close()
		return errors.New("I2P error: " + regexp.MustCompile(`MESSAGE=(.*)`).FindStringSubmatch(text)[1])
	} else {
		conn.Close()
		return errors.New("unable to parse SAMv3 reply: " + text)
	}
}

func (s *MasterSession) AddDatagramSession(id string, options []string) (*DatagramSession, error) {
	lhost, _, err := net.SplitHostPort(s.sam.conn.LocalAddr().String())
	if err != nil {
		s.sam.Close()
		return nil, err
	}
	lUDPAddr, err := net.ResolveUDPAddr("udp4", lhost+":0")
	if err != nil {
		return nil, err
	}
	udpconn, err := net.ListenUDP("udp4", lUDPAddr)
	if err != nil {
		return nil, err
	}
	_, lport, err := net.SplitHostPort(udpconn.LocalAddr().String())
	err = s.addGenericSession("DATAGRAM", id, options, []string{"HOST=" + lhost, "PORT=" + lport})
	if err != nil {
		return nil, err
	}
	ds := &DatagramSession{s.sam.address, id, s.sam.conn, udpconn, s.keys, s.rUDPAddr}
	s.subsessions[id] = ds
	return ds, nil
}

func (s *MasterSession) AddRawSession(id string, options []string) (*RawSession, error) {
	lhost, _, err := net.SplitHostPort(s.sam.conn.LocalAddr().String())
	if err != nil {
		s.sam.Close()
		return nil, err
	}
	lUDPAddr, err := net.ResolveUDPAddr("udp4", lhost+":0")
	if err != nil {
		return nil, err
	}
	udpconn, err := net.ListenUDP("udp4", lUDPAddr)
	if err != nil {
		return nil, err
	}
	_, lport, err := net.SplitHostPort(udpconn.LocalAddr().String())
	err = s.addGenericSession("RAW", id, options, []string{"HOST=" + lhost, "PORT=" + lport})
	if err != nil {
		return nil, err
	}
	rs := &RawSession{s.sam.address, id, s.sam.conn, udpconn, s.keys, s.rUDPAddr}
	s.subsessions[id] = rs
	return rs, nil
}

func (s *MasterSession) AddStreamSession(id string, options []string) (*StreamSession, error) {
	err := s.addGenericSession("STREAM", id, options, []string{})
	if err != nil {
		return nil, err
	}
	ss := &StreamSession{s.sam.address, id, s.sam.conn, s.keys}
	s.subsessions[id] = ss
	return ss, nil

}

// Closes the MasterSession.
func (s *MasterSession) Close() error {
	for id, subsession := range s.subsessions {
		if err := s.RemoveSession(id); err != nil {
			return err
		}
		if err := subsession.Close(); err != nil {
			return err
		} // is it good to be returning early if a single among the sessions fails to close?!
	}
	return nil
}

func (s *MasterSession) RemoveSession(id string) error {
	scmsg := []byte("SESSION REMOVE ID=" + id + " \n")
	for m, i := 0, 0; m != len(scmsg); i++ {
		if i == 15 {
			s.sam.Close()
			return errors.New("writing to SAM failed")
		}
		n, err := s.sam.conn.Write(scmsg[m:])
		if err != nil {
			s.sam.Close()
			return err
		}
		m += n
	}
	buf := make([]byte, 4096)
	n, err := s.sam.conn.Read(buf)
	if err != nil {
		s.sam.Close()
		return err
	}
	text := string(buf[:n])
	if strings.HasPrefix(text, session_OK) { // are the other outcomes still valid?
		return nil
	} else if text == session_DUPLICATE_ID {
		s.sam.Close()
		return errors.New("duplicate tunnel name")
	} else if text == session_DUPLICATE_DEST {
		s.sam.Close()
		return errors.New("duplicate destination")
	} else if text == session_INVALID_KEY {
		s.sam.Close()
		return errors.New("invalid key")
	} else if strings.HasPrefix(text, session_I2P_ERROR) {
		s.sam.Close()
		return errors.New("I2P error: " + regexp.MustCompile(`MESSAGE=(.*)`).FindStringSubmatch(text)[1])
	} else {
		s.sam.Close()
		return errors.New("unable to parse SAMv3 reply: " + text)
	}
}
